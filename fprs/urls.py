from __future__ import annotations
from django.urls import path, re_path
from . import views

app_name = "fprs"

urlpatterns = [
    # Manage fingerprints
    path('person/<key>/', views.PersonFingerprints.as_view(), name="person_list"),
    # Activate fingerprints
    re_path(r'^person/(?P<key>[^/]+)/(?P<fpr>[0-9A-F]+)/activate/$', views.SetActiveFingerprint.as_view(),
            name="person_activate"),
]
