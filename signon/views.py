from __future__ import annotations
from django.conf import settings
from django.views.generic import View
from django.contrib.auth import get_user_model
from django import http
from django.shortcuts import redirect
from django.contrib import auth
from .models import Identity
from . import providers


class LoginMixin:
    """
    Mixin to plug into a TemplateView to handle logins
    """
    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        active = []
        inactive = []
        for provider in (x.bind(self.request) for x in getattr(settings, "SIGNON_PROVIDERS", ())):
            if provider.get_active_identity() is not None:
                active.append(provider)
            else:
                inactive.append(provider)
        ctx["providers_active"] = active
        ctx["providers_inactive"] = inactive
        if self.request.user.is_authenticated:
            ctx["identities"] = self.request.user.identities.all()
        return ctx


class Logout(View):
    def post(self, request, *args, **kw):
        # Remove all Identities currently in session
        for key in [k for k in request.session.keys() if k.startswith("signon_identity_")]:
            del request.session[key]

        # If we at some point have providers that require actions on logout,
        # call a hook on all providers of active identities here

        if request.user.is_authenticated:
            # Log out the Django user if they were logged in.
            auth.logout(request)

        next_url = request.POST.get("next")
        if next_url is None:
            return redirect("home")
        else:
            return redirect(next_url)


class Whoami(View):
    def get(self, request, *args, **kw):
        identities = {}
        for name, identity in request.signon_identities.items():
            identities[name] = {
                "issuer": identity.issuer,
                "subject": identity.subject,
                "last_used": identity.last_used.strftime("%Y-%M-%d"),
                "profile": identity.profile,
                "picture": identity.picture,
                "fullname": identity.fullname,
                "username": identity.username,
            }
        return http.JsonResponse({
            "user": {
                "pk": self.request.user.pk,
                "str": str(self.request.user),
            },
            "identities": identities,
        })


class OIDCAuthenticationCallbackView(View):
    def get(self, request, *args, **kw):
        User = get_user_model()
        name = self.kwargs["name"]
        provider = providers.get(name, None)
        if provider is None:
            raise http.Http404

        provider = provider.bind(request)
        provider.load_tokens()

        info = provider.get_userinfo()

        claims = provider.id_token_claims
        try:
            identity = Identity.objects.get(issuer=name, subject=claims["sub"])
        except Identity.DoesNotExist:
            identity = Identity.objects.create(
                    issuer=name, subject=claims["sub"],
                    audit_author=User.objects.get_housekeeper(),
                    audit_notes="Identity created automatically on successful OIDC authentication")

        changed = identity.update(
            profile=info.get("profile"),
            picture=info.get("picture"),
            fullname=info.get("name"),
            username=info.get("nickname"),
            audit_author=User.objects.get_housekeeper(),
            audit_notes="updated from login user information")

        if not changed:
            # Save to update last_used
            identity.save(audit_skip=True)

        request.session[f"signon_identity_{name}"] = identity.pk
        return redirect("home")
