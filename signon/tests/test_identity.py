from __future__ import annotations
from django.test import TestCase
from signon.unittest import SignonFixtureMixin
from signon.models import Identity
import json


class TestIdentity(SignonFixtureMixin, TestCase):
    def test_create(self):
        # Create an unbound identity
        identity = Identity.objects.create(
            issuer="debsso", subject="user1@debian.org", username="user1@debian.org",
            audit_author=self.user1, audit_notes="created test identity")

        self.assertIsNone(identity.person)

        log = list(identity.audit_log.all())
        self.assertEqual(len(log), 1)
        log = log[0]
        self.assertEqual(log.identity, identity)
        self.assertEqual(log.author, self.user1)
        self.assertEqual(log.notes, "created test identity")
        self.assertEqual(json.loads(log.changes), {
            'fullname': [None, ''],
            'issuer': [None, 'debsso'],
            'person': [None, None],
            'picture': [None, ''],
            'profile': [None, ''],
            'subject': [None, 'user1@debian.org'],
            'username': [None, 'user1@debian.org'],
        })

        self.assertEqual(str(identity), "-:debsso:user1@debian.org (aka user1@debian.org)")

        # Bind it
        identity.person = self.user1
        identity.save(audit_author=self.user1, audit_notes="Bound to person")

        log = identity.audit_log.order_by("-logdate", "-id").first()
        self.assertEqual(log.identity, identity)
        self.assertEqual(log.author, self.user1)
        self.assertEqual(log.notes, "Bound to person")
        self.assertEqual(json.loads(log.changes), {
            'person': [None, str(self.user1)],
        })

        self.assertEqual(
            str(identity),
            f"{self.user1}:debsso:user1@debian.org (aka user1@debian.org)"
        )
