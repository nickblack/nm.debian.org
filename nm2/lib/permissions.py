class Permission:
    """
    Describe a permission for a Perms class
    """
    def __init__(self, doc):
        self.doc = doc


class PermissionsMetaclass(type):
    def __new__(cls, clsname, bases, dct):
        perms = {}

        # Add perms from subclasses
        for b in bases:
            if not issubclass(b, Permissions):
                continue
            b_perms = getattr(b, "_meta", None)
            if b_perms is None:
                continue
            perms.update(b_perms)

        slots = ()
        for name, val in list(dct.items()):
            if isinstance(val, Permission):
                perms[name] = val
                del dct[name]
            elif name == "__slots__":
                slots = val
                del dct[name]

        dct["__slots__"] = tuple(slots) + tuple(perms.keys())
        dct["_meta"] = perms

        res = super().__new__(cls, clsname, bases, dct)
        return res


class Permissions(metaclass=PermissionsMetaclass):
    """
    Base class for sets of computed permissions.

    Permission checking is stricter than a normal set: misspelled permissions
    raise AttributeError instead of silently failing.
    """
    __slots__ = ()

    def __init__(self, **kw):
        for name in self._meta.keys():
            setattr(self, name, kw.pop(name, False))

        if kw:
            raise AttributeError("{} cannot be used with perms".format(", ".join(sorted(kw.keys()))))

    def __contains__(self, key):
        """
        Check if a permission is set (legacy set membership interface)
        """
        return getattr(self, key)

    def __iter__(self):
        """
        Return the list of set permissions
        """
        for name in self._meta.keys():
            if getattr(self, name):
                yield name
