from django.urls import include, path
from django.views.generic import TemplateView
from backend.mixins import VisitorTemplateView
from django.conf import settings
from rest_framework import routers
from django.contrib import admin
import django.conf.urls
import backend.views as bviews
import process.views as pviews
import public.views as public_views
import nmlayout.views as nmviews

admin.autodiscover()

router = routers.DefaultRouter()
router.register(r'persons', bviews.PersonViewSet, "persons")
router.register(r'processes', pviews.ProcessViewSet, "processes")

django.conf.urls.handler403 = nmviews.Error403.as_view()
django.conf.urls.handler404 = nmviews.Error404.as_view()
django.conf.urls.handler500 = nmviews.Error500.as_view()


urlpatterns = [
    path('robots.txt', TemplateView.as_view(
        template_name='robots.txt', content_type="text/plain"), name="root_robots_txt"),
    path('i18n/', include('django.conf.urls.i18n')),
    path(r'', VisitorTemplateView.as_view(template_name='index.html'), name="home"),
    path('license/', VisitorTemplateView.as_view(template_name='license.html'), name="root_license"),
    path('faq/', VisitorTemplateView.as_view(template_name='faq.html'), name="root_faq"),
    path('login/', nmviews.Login.as_view(), name='login'),
    path('members/', public_views.Members.as_view(), name="members"),
    path('legacy/', include("legacy.urls")),
    path('person/', include("person.urls")),
    path('public/', include("public.urls")),
    path('am/', include("restricted.urls")),
    path('fprs/', include("fprs.urls")),
    path('process/', include("process.urls")),
    path('dm/', include("dm.urls")),
    path('api/', include("api.urls")),
    path('apikeys/', include("apikeys.urls")),
    path('keyring/', include("keyring.urls")),
    path('wizard/', include("wizard.urls")),
    path('mia/', include("mia.urls")),
    path('minechangelogs/', include("minechangelogs.urls")),
    path('sitechecks/', include("sitechecks.urls")),
    path('deploy/', include("deploy.urls")),
    path('rest/api/', include(router.urls)),
    path('signon/', include("signon.urls")),
    path('impersonate/', include("impersonate.urls")),

    # Uncomment the admin/doc line below to enable admin documentation:
    path('admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    try:
        import debug_toolbar
        urlpatterns += [
            path('__debug__/', include(debug_toolbar.urls)),
        ]
    except ImportError:
        pass
